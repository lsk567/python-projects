#/usr/bin/python
# -*- coding: utf-8 -*-

import random

class Card(object):  
    
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank        

    def __str__(self):
        return self.suit+self.rank

    def value(self, total):
        rank = self.rank
        value = 0
        try:
            value = int(rank)
        except ValueError:
            if rank == 'J' or rank == 'Q' or rank == 'K':
                value = 10
            elif rank == 'A':
                if total > 11:
                    value = 1
                else:
                    value = 10
            else:
                #error case
                print("Invalid rank.")
                value = 0
        return value


def make_deck():

    suits = ['♠','♣','♦','♥']
    ranks = ['A','2','3','4','5','6','7','8','9','10','J','Q','K']
    deck = []

    for i in range(13):
        for j in range(4):
            deck.append(Card(suits[j],ranks[i]))

    # use random.shuffle(some_list) to shuffle the deck of cards.
    random.shuffle(deck)

    return deck


def draw_card(deck, current_score):
    card = deck.pop()
    current_score += card.value(current_score)
    return card, current_score


def main():
    deck = make_deck()

    human_score = 0
    ai_score = 0     

    user_input = ""
    while user_input != "n":
        #draw a card for user
        card_drawn, human_score = draw_card(deck, human_score)
        print("You drew "+card_drawn.__str__())
        print("sum: "+str(human_score))

        #if exceeds 21, user loses
        if human_score > 21:
            print("Score exceeds 21. You lose.")
            break
        elif human_score == 21:
            print("21! You win.")
            break

        user_input = input("Do you want to draw another card? (y/n) ")

    if human_score < 21:
        print("My turn")
        while ai_score < 17:
            #draw a card for user
            card_drawn, ai_score = draw_card(deck, ai_score)
            print("I drew "+card_drawn.__str__())
            print("sum: "+str(ai_score))

            #if exceeds 21, user loses
            if ai_score > 21:
                print("Score exceeds 21. I lose.")
                break
            elif ai_score == 21:
                print("21! I win.")
                break

        #no one gets to exactly 21 but still both in play
        if ai_score < 21: 
            if human_score == ai_score:
                print("Push. Nobody wins.")
            elif human_score > ai_score:
                print("You win.")
            else:
                print("I win.")


if __name__ == "__main__":
    main()
