def read_markets(filename):
    """
    Read in the farmers market data from the file named filename and return 
    a tuple of two objects:
    1) A dictionary mapping zip codes to lists of farmers market tuples.
    2) A dictionary mapping towns to sets of zip codes.
    """

    zip_to_market = {}
    town_to_zips = {}

    file_handle = open(filename, "r")

    line = file_handle.readline()
    while line != "":
        #put market info in a list
        market = tuple(line.strip().split("#")[:5])

        #populate zip_to_market dictionary
        if market[4] in zip_to_market.keys():
            zip_to_market[market[4]].append(market)
        else:
            zip_to_market[market[4]] = []
            zip_to_market[market[4]].append(market)

        #populate town_to_zips
        town_lower = market[3].lower()
        if town_lower in town_to_zips.keys():
            if market[4] not in town_to_zips[town_lower]:
                town_to_zips[town_lower].append(market[4])
        else:
            town_to_zips[town_lower] = []
            town_to_zips[town_lower].append(market[4])

        line = file_handle.readline()

    file_handle.close()

    return zip_to_market, town_to_zips

def print_market(market):
    """
    Returns a human-readable string representing the farmers market tuple
    passed to the market parameter.
    """
    return "\n"+market[1]+"\n"+market[2]+"\n"+market[3]+", "+market[0]+" "+market[4]


if __name__ == "__main__":

    # This main program first reads in the markets.txt once (using the function
    # from part (a)), and then asks the user repeatedly to enter a zip code or
    # a town name (in a while loop until the user types "quit").

    FILENAME = "markets.txt"

    try: 
        zip_to_market, town_to_zips = read_markets(FILENAME)

        # your main loop should be here

    except (FileNotFoundError, IOError): 
        print("Error reading {}".format(FILENAME))

    user_input = (input("Please enter a zip code or a town name: ")).lower()
    while not user_input == "quit":
        market_list = []
        try:
            #if input zip code, no error
            user_input = str(int(user_input))

            if user_input in zip_to_market.keys():

                for market in zip_to_market[user_input]:
                        
                    print(print_market(market))
            else:
                print("Not found.")



        except ValueError:
            #if user enter town
            if user_input in town_to_zips.keys():

                for zipcode in town_to_zips[user_input]:

                    for market in zip_to_market[zipcode]:
                        
                        print(print_market(market))
            else:
                print("Not found")

        user_input = (input("Please enter a zip code or a town name: ")).lower()

    print("Bye")










