from collections import defaultdict

def count_ngrams(file_name, n=2): 
	"""
	This function reads an input file and returns a dictionary of n-gram counts.  
	file_name is a string, n is an integer. 
	The result dictionary maps n-grams to their frequency (i.e. the count of 
	how often that n-gram appears). Each n-gram key is a tuple and the count is
	an int.
	"""
	# The defaultdict class may be useful here. Check the python 
	# documentation for more information: 
	# https://docs.python.org/3/library/collections.html#collections.defaultdict

	try:
		file_handle = open(file_name, "r")
	except FileNotFoundError:
		print('File does not exist.')
		return {}

	file_str = ""

	line = file_handle.readline()
	line = line.strip()
	while line != "":
		for ch in line.lower():
			if ch != ',' and ch != '.' and ch != ';' and ch != '\"' and ch != '?' and ch != '!':
				file_str += ch
		file_str += ' '
		line = file_handle.readline()

	words = file_str.split()
	gram_dict = {}

	if n < 1 or n > len(words):
		print("Invalid ngram parameter!")
		return {}

	idx = 0

	while idx + n <= len(words):
		gram = []
		for i in range(n):
			gram.append(words[idx+i])
		gram = tuple(gram)
		
		if gram in gram_dict.keys():
			gram_dict[gram] += 1
		else:
			gram_dict[gram] = 1
		

		idx += 1

	
	return gram_dict



def single_occurences(ngram_count_dict): 
	"""
	This functions takes in a dictionary (in the format produces by 
	count_ngrams) and returns a list of all ngrans with only 1 occurence.
	That is, this function should return a list of all n-grams with a count
	of 1. 
	"""
	singles = []
	for key, value in ngram_count_dict.items():
		if value == 1:
			singles.append(key)
	return singles

def most_frequent(ngram_count_dict, num = 5): 
	"""
	This function takes in two parameters: 
		ngram_count_dict is a dictionary of ngram counts. 
		num denotes the number of n-grams to return.	   
	This function returns a list of the num n-grams with the highest
	occurence in the file. For example if num=10, the method should 
	return the 10 most frequent ngrams in a list. 
	"""
	# Hint: For this you will need to sort the information in the dict 
	# Python does not support any way of sorting dicts 
	# You will have to convert the dict into a list of (frequency, n-gram)
	# tuples, sort the list based on frequency, and return a list of the num
	# n-grams with the highest frequency. 
	# NOTE: you should NOT return the frequencies, just a list of the n-grams

	tuple_list = []
	for key, value in ngram_count_dict.items():
		tuple_list.append((value, key))

	tuple_list.sort()
	# print(tuple_list)

	tuple_list = list(reversed(tuple_list[(-1*num):]))
	# print(tuple_list)

	high_freq = []
	for item in tuple_list:
		high_freq.append(item[1])


	return high_freq

def main():
	filename = "alice.txt"
	n = 2
	most_frequent_k = 5

	ngram_counts = count_ngrams(filename, n)
	print(ngram_counts)

	
	print('{}-grams that occur only once:'.format(n))
	print(single_occurences(ngram_counts))
	
	print('{} most frequent {}-grams:'.format(most_frequent_k, n))
	print(most_frequent(ngram_counts, most_frequent_k))
	

if __name__ == "__main__":
	main()
