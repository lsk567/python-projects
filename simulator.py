import random
import math
from matplotlib import pyplot as plt

def normpdf(x, mean, sd):
    """
    Return the value of the normal distribution 
    with the specified mean and standard deviation (sd) at
    position x.
    You do not have to understand how this function works exactly. 
    """
    var = float(sd)**2
    denom = (2*math.pi*var)**.5
    num = math.exp(-(float(x)-float(mean))**2/(2*var))
    return num/denom

def pdeath(x, mean, sd):
    start = x-0.5
    end = x+0.5
    step =0.01    
    integral = 0.0
    while start<=end:
        integral += step * (normpdf(start,mean,sd) + normpdf(start+step,mean,sd)) / 2
        start += step            
    return integral    
    
recovery_time = 3 # recovery time in time-steps
virality = 0.8    # probability that a neighbor cell is infected in 
                  # each time step   
mean = .3         # death rate settings:
sd = .3                                               

class Cell(object):

    def __init__(self,x, y):
        self.x = x
        self.y = y 
        self.state = "S" # can be "S" (susceptible), "R" (resistant = dead), or 
                         # "I" (infected)
        self.recover_count = 0
        
    def infect(self):
        self.state = "I"
        
    def process(self, adjacent_cells):
        
        if self.state == "I":
            
            #check if it recovers
            self.recover_count += 1
            if (self.recover_count == 4):
                self.state = "S"
                self.recover_count = 0
                return
            
            #check if it dies
            num = random.random()
            if num < pdeath(self.recover_count, mean, sd):
                self.state = "R"
                self.recover_count = 0
                return
                
            for cell in adjacent_cells:
                if cell.state == "S":
                    #determine whether they are infected or not
                    num = random.random()
                    if num < virality:
                        cell.infect()
                    
            
            
        
class Map(object):
    
    def __init__(self):
        self.height = 150
        self.width = 150           
        self.cells = {}

    def add_cell(self, cell):
        #populate dictionary
        self.cells[(cell.x, cell.y)] = cell
        
    def display(self):
        #initialize a 2d array
        image=[[(0.0,0.0,0.0) for j in range(150)] for i in range(150)]
        #change color based on cell status
        for coord, cell in self.cells.items():
            if (cell.state == "S"):
                image[coord[0]][coord[1]] = (0.0,1.0,0.0)
            elif (cell.state == "R"):
                image[coord[0]][coord[1]] = (0.5,0.5,0.5)
            elif (cell.state == "I"):
                image[coord[0]][coord[1]] = (1.0,0.0,0.0)
            else:
                #error case: white
                image[coord[0]][coord[1]] = (1.0,1.0,1.0)
            #print(coord)
        #print(self.cells)
        plt.imshow(image)
    
    def adjacent_cells(self, x,y):
        #print("x,y = "+x+","+y)
        coord = []
        if x < 149:
            if (x+1, y) in self.cells.keys():
                coord.append(self.cells[(x+1, y)])
                #print("x,y = "+x+1+","+y+" added")
        if x > 0:
            if (x-1, y) in self.cells.keys():
                coord.append(self.cells[(x-1, y)])
                #print("x,y = "+(x-1)+","+y+" added")
        if y < 149:
            if (x, y+1) in self.cells.keys():
                coord.append(self.cells[(x, y+1)])
                #print("x,y = "+x+","+(y+1)+" added")
        if y > 0:
            if (x, y-1) in self.cells.keys():
                coord.append(self.cells[(x, y-1)])
                #print("x,y = "+x+","+(y-1)+" added")
            
        return coord
     
    def time_step(self):
        for coord, cell in self.cells.items():
            cell.process(self.adjacent_cells(coord[0],coord[1]))
        
        self.display()

            
def read_map(filename):
    #create empty map
    m = Map()
    
    #read coordinates, add cell to map
    fileHandle = open(filename, 'r')
    line = fileHandle.readline()
    while (line != ''):
        coordinates = line.strip().split(',')
        m.add_cell(Cell(int(coordinates[0]),int(coordinates[1])))
        line = fileHandle.readline()
        
    #first infected cell!
    m.cells[(80, 75)].state = "I"
    
    return m
